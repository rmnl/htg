# Happy Tree Gallery

This is a small utility for generating static photo galleries.

**BEWARE THIS IS STILL VERY MUCH A WORK IN PROGRESS**

And that's why there's no documentation yet. Please check back later.


## Dependencies

### jpegtran

Installing jpegtran is easy:

```bash
brew install jpeg
```
