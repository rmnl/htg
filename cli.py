import click

from htg.htg import htg


if __name__ == "__main__":
    click.echo("")
    click.echo(80 * "#")
    click.echo((27 * "#") + " RUNNING FROM SOURCE FILE " + (27 * "#"))
    click.echo(80 * "#")
    click.echo("")
    htg()
