# Showcase CLI Changelog

This is the changelog for the `htg` command line utility. All notable changes are kept here in a human readable form. It's based on the [keepachangelog.com](https://keepachangelog.com/en/1.0.0/) guidelines.

This project adheres to the [PEP-0440 standard](https://www.python.org/dev/peps/pep-0440/) for version identification and dependency specification.

**NB**: For release dates, please look at the [release overview](https://github.com/jwplayer/showcase-management/releases).

